class Task < ActiveRecord::Base
  include Schedulable

  validates_presence_of :executable_path, :agent, :agent_key, :scheduled_days
  validates_length_of :executable_path, :agent_key, :agent, minimum: 3

  # dummy days field to hold selected days
  attr_accessor :days

  # scheduled tasks that have not yet been executed
  scope :for_today, -> { where('scheduled_days like ?', "%#{Date.today.strftime('%A')[1...3]}%") }

  # sets the scheduled_days to the ones stored on the #days attribute
  def set_days
    self.scheduled_days = self.days.drop(1).collect { |s| s[0...3] }.join(',').upcase # drop the first param as it comes nil
  end

end
