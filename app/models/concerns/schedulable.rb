module Schedulable
  extend ActiveSupport::Concern

  require 'rufus-scheduler'
  require 'net/http'


  # Schedules the task using rufus-scheduler
  def schedule
    s = Rufus::Scheduler.new

    # assign job id to the instance including this module
    self.job_id =
        s.cron cron_script() do
          call_agent
        end

    # update the model
    self.save!
    puts "Created job: #{self.job_id}"

  end

  # calls this task's agent
  def call_agent
    puts 'Running job: ' +self.to_s
    puts "Calling agent on #{agent}"

    url = URI.parse self.agent
    response = Net::HTTP.post_form url, { :agent_key => self.agent_key, :execution_path => self.executable_path, :task_name => self.name }

    puts "Received response from agent: #{response}"
  end

  # Returns the corresponding cron_script for this Task
  def cron_script
    days = self.scheduled_days
    t = self.start_time
    sc = "%d %d %d * * %s" % [t.sec, t.min, t.hour, days]
    puts "Scheduling cron job (#{sc})"
    sc
  end

  # Looks up job id and unschedules it
  def unschedule
    s = Rufus::Scheduler.new
    job = s.job self.job_id

    #inly unschedule if we can find the job
    if not job.nil?
      job.unschedule

      puts "Destroyed job: #{self.job_id}"

      # update the model
      self.save!
    else
      puts "The job (#{self.job_id}) was not found!"
    end

  end


end