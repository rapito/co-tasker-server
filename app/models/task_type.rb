class TaskType < ActiveRecord::Base
  validates_presence_of :name, :description
  validates_length_of :name, :description, minimum: 5
end
