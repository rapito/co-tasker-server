json.array!(@tasks) do |task|
  json.extract! task, :id, :name, :executable_path, :start_time, :end_time, :scheduled_days, :agent
  json.url task_url(task, format: :json)
end
