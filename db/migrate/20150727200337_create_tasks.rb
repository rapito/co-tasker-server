class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :name
      t.string :executable_path
      t.time :start_time
      t.time :end_time
      t.string :scheduled_days
      t.string :agent
      t.string :agent_key

      t.timestamps null: false
    end
  end
end
