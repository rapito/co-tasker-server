Rails.application.routes.draw do
  get '/' => 'main#index'

  resources :task_types
  resources :tasks

end
